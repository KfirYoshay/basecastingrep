import base_casting


def test_generate_base_characters_list_16():
    result_list = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F']
    assert result_list == base_casting.generate_base_characters_list(16)


def test_generate_base_characters_list_2():
    result_list = ['0', '1']
    assert result_list == base_casting.generate_base_characters_list(2)


def test_check_if_num_is_in_src_base_16():
    base_list = base_casting.generate_base_characters_list(16)
    assert base_casting.check_if_num_is_in_src_base("DeadBeef", base_list) is True


def test_check_if_num_is_in_src_base_36():
    base_list = base_casting.generate_base_characters_list(36)
    assert base_casting.check_if_num_is_in_src_base("1PS9wXB", base_list) is True


def test_check_if_num_is_in_src_base_15():
    base_list = base_casting.generate_base_characters_list(15)
    assert base_casting.check_if_num_is_in_src_base("DEADBEEF", base_list) is False


def test_validate_base_1():
    assert base_casting.validate_base(1) is False


def test_validate_base_2():
    assert base_casting.validate_base(2) is True


def test_validate_base_3():
    assert base_casting.validate_base("asdasd") is False


def test_cast_from_base_to_base_1():
    assert base_casting.cast_from_base_to_base("DEADBEEF", 16, 10) == "3735928559"


def test_cast_from_base_to_base_2():
    assert base_casting.cast_from_base_to_base("1PS9wXB", 36, 16) == "DEADBEEF"


def test_cast_from_base_to_base_3():
    assert base_casting.cast_from_base_to_base("DEADBEEF", 15, 10) is None


def test_cast_from_base_to_base_4():
    assert base_casting.cast_from_base_to_base("DEADBEEF", 49, 10) is None


def test_cast_from_base_to_base_5():
    assert base_casting.cast_from_base_to_base("DEADBEEF", 16, 2) == "11011110101011011011111011101111"
