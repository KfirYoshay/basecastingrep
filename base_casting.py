FIRST_DIGIT = '0'
LAST_DIGIT = '9'
FIRST_CHARACTER = 'A'
LAST_CHARACTER = 'Z'


def generate_base_characters_list(base):
    """This function will receive base number and return a list of characters this case contains"""
    base_characters = list(filter(lambda y: y <= LAST_DIGIT or y >= FIRST_CHARACTER,
                                  map(lambda x: chr(x), range(ord(FIRST_DIGIT), ord(LAST_CHARACTER) + 1, 1))))
    return base_characters[:base]


def check_if_num_is_in_src_base(number_to_check, src_base_characters_list):
    """This function will receive number as string and base characters list and will check if"""
    """there is a character from the number that not exist in the list"""
    return not any(char not in src_base_characters_list for char in number_to_check.upper())


def convert_number_to_base_10(number_to_convert, src_base_to_convert, src_base_characters_list):
    """This function will receive number as string, source base to convert from and base characters list"""
    """and will return the number in base 10"""
    base_10_value = 0
    power_index = len(number_to_convert) - 1
    for char in number_to_convert.upper():
        char_as_number = src_base_characters_list.index(char)
        base_10_value += char_as_number * (src_base_to_convert ** power_index)
        power_index -= 1
    return base_10_value


def convert_base_10_number_to_dst_base(base_10_number, dst_base_to_convert):
    """This function will receive number in base 10 and will convert it to the destination base"""
    dst_base_characters_list = generate_base_characters_list(dst_base_to_convert)
    dst_base_number_list = []
    while base_10_number is not 0:
        leftover = base_10_number % dst_base_to_convert
        dst_base_number_list.insert(0, dst_base_characters_list[leftover])
        base_10_number = base_10_number // dst_base_to_convert
    return "".join(dst_base_number_list)


def validate_base(base):
    """This function will validate that the given base is a number and in range 2 -> 36"""
    if not str(base).isdigit():
        print("The base {} is not a number".format(base))
        return False
    base_numeric = int(base)
    if base_numeric < 2 or base_numeric > 36:
        print("Base {} is not a legal base".format(base))
        return False
    return True


def cast_from_base_to_base(num_as_string, src_base, dst_base):
    """This function will receive number as string in given source base and will return the converted number in"""
    """In the given destination base"""
    """"""
    if not validate_base(src_base):
        return None
    if not validate_base(dst_base):
        return None
    numeric_src_base = int(src_base)
    numeric_dst_base = int(dst_base)
    src_base_characters_list = generate_base_characters_list(numeric_src_base)
    upper_num_as_string = num_as_string.upper()
    if not check_if_num_is_in_src_base(upper_num_as_string, src_base_characters_list):
        print("The number {} is not a legal number in base {}".format(num_as_string, src_base))
        return None
    print("{} is a valid number in base {}".format(num_as_string, src_base))
    base_10_number = convert_number_to_base_10(upper_num_as_string, numeric_src_base, src_base_characters_list)
    if dst_base == 10:
        return str(base_10_number)
    return convert_base_10_number_to_dst_base(base_10_number, numeric_dst_base)


if __name__ == "__main__":
    number_as_string = input("Enter string number to convert: ")
    source_base = input("Enter source base: ")
    destination_base = input("Enter destination base: ")
    dst_base_number = cast_from_base_to_base(number_as_string, source_base, destination_base)
    if dst_base_number is not None:
        print("Its representation in destination base ({}) is: {}".format(destination_base, dst_base_number))
